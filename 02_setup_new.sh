#!/bin/bash

read -p "Please enter the name you would like for this set of keys: " config_name
read -p "Please enter the comment (email) to use with this key: " comment

if [ -z "$config_name" ]; then
    echo "ERROR: Config name cannot be empty"
    exit 1
fi

if [ -z "$comment" ]; then
    echo "ERROR: Comment cannot be empty"
    exit 1
fi


private_key_name="id_rsa.$config_name"
public_key_name="id_rsa.$config_name.pub"

echo "---------- Generating keys -----------"
ssh-keygen -t rsa -C "$comment" -f "$private_key_name"
echo "--------------------------------------"

mv "$private_key_name" ~/.ssh/
mv "$public_key_name" ~/.ssh/

echo "Keys generated to $private_key_name and $public_key_name"

# Set up scripts
if [ ! -d "scripts" ]; then
    echo "scripts/ directory doesn't exist. Creating..."
    mkdir scripts
fi
cp setup_scripts/CONFIGNAME_ssh.sh scripts/${config_name}_ssh.sh
echo "Created scripts/${config_name}_ssh.sh. Next, run 02_setup_new.sh to create a new key or run 03_install.sh to copy all scripts to your home directory."