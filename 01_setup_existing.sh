#!/bin/bash
# Renames existing ~/.ssh/id_rsa and id_rsa.pub for use with these scripts.

# Error if no existing scripts
if [ -d ~/.ssh/ ] && [ -f ~/.ssh/id_rsa ] && [ -f ~/.ssh/id_rsa.pub ]; then
	echo "Existing keys discovered"

	read -p "Please enter the name you would like for this set of keys. Comment is \"$(cat ~/.ssh/id_rsa.pub | awk '{print $3}')\": " config_name

	private_key_name="id_rsa.$config_name"
	public_key_name="id_rsa.$config_name.pub"

	mv ~/.ssh/id_rsa ~/.ssh/$private_key_name
	mv ~/.ssh/id_rsa.pub ~/.ssh/$public_key_name

	echo "Keys renamed to $private_key_name and $public_key_name"

	# Set up scripts
	if [ ! -d "scripts" ]; then
		echo "scripts/ directory doesn't exist. Creating..."
		mkdir scripts
	fi
	cp setup_scripts/CONFIGNAME_ssh.sh scripts/$config_name_ssh.sh
	echo "Created scripts/$config_name_ssh.sh. Next, run 02_setup_new.sh to create a new key or run 03_install.sh to copy all scripts to your home directory."
	
else
	echo "Error: No existing keys discovered. Please generate them."
fi
