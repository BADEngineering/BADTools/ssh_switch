#!/bin/bash
# Activates ssh key pair specified by the script name:
#    <config_name>_ssh.sh

user="$(echo $0 | awk '{split($0,a,"_"); split(a[1],b,"/"); print b[2]}')"

cd ~/.ssh
cp id_rsa.$user id_rsa
cp id_rsa.$user.pub id_rsa.pub
echo "Activated $user ssh keys!"
